﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMovie : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	public float speed = 4.0f;
	public float turnSpeed = 180.0f;
	public Transform target;
	public Vector2 heading = Vector3.right;

	// Update is called once per frame
	void Update () {
		Vector2 direction = target.position - transform.position;

		float angle = turnSpeed * Time.deltaTime;

		if (direction.IsOnLeft (heading)) {
			heading = heading.Rotate (angle);
		} else {
			heading = heading.Rotate (-angle);
		}
			
		transform.Translate (heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, heading);

		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay (transform.position, direction);
	}
}
