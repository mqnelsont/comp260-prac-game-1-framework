﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveWK3 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	public float maxSpeed = 5.0f; //metres per sec
	public float acceleration = 4.0f; //metres/second/second
	public float brake = 5.0f; //metres/second/second
	public float turnSpeed = 30.0f; //degrees/second
	private float speed = 0.0f; //metres/second


	// Update is called once per frame
	void Update () {
		//the horizontal axis controls the turn
		float turn = Input.GetAxis("Horizontal");

		//turn the car
		transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime);

		//the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis("Vertical");
		if (forwards > 0) {
			//accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		} else if (forwards < 0) {
			//accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		} else {
			//braking
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
			} else {
				speed = speed + brake * Time.deltaTime;
			}
		}

		//clamp the speed, brake and turnSpeed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
		brake = Mathf.Clamp (brake, 1.0f, 7.0f);
		turnSpeed = Mathf.Clamp (turnSpeed, 10.0f, 50.0f);

		//compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		//move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}
