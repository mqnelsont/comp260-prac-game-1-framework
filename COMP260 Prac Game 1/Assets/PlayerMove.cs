﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	public float maxSpeed = 5.0f;

	void player1(){
		Vector2 direction;

		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");

		Vector2 velocity = direction * maxSpeed;
		transform.Translate (velocity * Time.deltaTime);
	}

	void player2(){
		Vector2 direction;

		direction.x = Input.GetAxis ("Horizontal2");
		direction.y = Input.GetAxis ("Vertical2");

		Vector2 velocity = direction * maxSpeed;
		transform.Translate (velocity * Time.deltaTime);
	}

	void Update () {
		player1 ();
		player2 ();
	}
}
